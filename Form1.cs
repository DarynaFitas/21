namespace an
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public static int Factorial(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int a = int.Parse(textBox2.Text);
           int n = int.Parse(textBox3.Text);
            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                double term = 1;
                for (int j = 1; j <= i; j++)
                {
                    term *= j;
                }
                term /= Math.Pow(a, i + 1);
                for (int k = 1; k <= i; k++)
                {
                    term *= (a + k - 1);
                }
                sum += term;
            }
            label4.Text = sum.ToString();   
        }
    }
}